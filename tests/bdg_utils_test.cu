//===-------- bdg_utils_test.cu - Unit tests of BdG related functions -----===//
//
// Part of the SuperConga Project, under the GNU LGPL v3 license or higher.
// See LICENSE.txt for license information.
//
//===----------------------------------------------------------------------===//
///
/// \file bdg_utils_test.cu
/// \brief Different tests related to the BdG theory of superconductivity.
///
//===----------------------------------------------------------------------===//
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "conga/defines.h"
#include "conga/fermi_surface/DispersionTightBinding.h"
#include "conga/fermi_surface/bdg_utils.h"
#include "conga/order_parameter/OrderParameterBasis.h"
#include "conga/utils.h"
#include "conga/wrappers/doctest.h"

#include <cmath>
#include <limits>
#include <vector>

namespace helper {
/// \brief Test the critical temperature within BdG.
///
/// Compare it with the results in the BdG paper:
/// https://arxiv.org/abs/2006.00456
///
/// \param InUseFermiSurface1 - Whether to use Fermi surface 1 as defined in the
///                             paper.
/// \param inTolerance - The relative tolerance. It is high because the results
///                      in the paper does not have that many significant
///                      digits.
///
/// \return - Void.
template <typename T>
void testComputeCriticalTemperature(
    const bool InUseFermiSurface1, const T inTolerance = static_cast<T>(1e-3)) {
  // =================== ARRANGE ==================== //
  T hoppingNNN;
  T hoppingNNNN;
  T chemicalPotential;
  T potential;
  T expectedDWaveTc;
  T expectedSExtWaveTc;

  if (InUseFermiSurface1) {
    // Dispersion parameters.
    hoppingNNN = -static_cast<T>(0.25);
    hoppingNNNN = static_cast<T>(0.0);
    chemicalPotential = static_cast<T>(0.0);
    potential = static_cast<T>(1.4);

    // Expeted critical temperatures.
    expectedDWaveTc = static_cast<T>(0.118);
    expectedSExtWaveTc = static_cast<T>(0.0);
  } else {
    // Dispersion parameters.
    hoppingNNN = -static_cast<T>(0.495);
    hoppingNNNN = static_cast<T>(0.156);
    chemicalPotential = -static_cast<T>(1.267);
    potential = static_cast<T>(1.279);
    expectedDWaveTc = static_cast<T>(0.114);
    expectedSExtWaveTc = static_cast<T>(0.0);
  }

  // Define the dispersion.
  conga::DispersionTightBinding<T> dispersion(hoppingNNN, hoppingNNNN,
                                              chemicalPotential);

  // D-wave and extended s-wave.
  const conga::Symmetry dWaveSymmetry = conga::Symmetry::DWAVE_X2_Y2;
  const conga::Symmetry sExtSymmetry = conga::Symmetry::SWAVE;

  // =================== ACT ==================== //
  const auto [dWaveSuccess, dWaveTc] = conga::bdg::computeCriticalTemperature(
      dispersion, potential, dWaveSymmetry);
  const auto [sExtWaveSuccess, sExtWaveTc] =
      conga::bdg::computeCriticalTemperature(dispersion, potential,
                                             sExtSymmetry);

  // =================== ASSERT ==================== //
  CHECK(dWaveSuccess);
  CHECK(dWaveTc == doctest::Approx(expectedDWaveTc).epsilon(inTolerance));
  CHECK(sExtWaveSuccess);
  CHECK(sExtWaveTc == doctest::Approx(expectedSExtWaveTc).epsilon(inTolerance));
}
} // namespace helper

// ============== Test computeCriticalTemperature ============== //

TEST_CASE("Test computeCriticalTemperature<float> - FS1 -") {
  const bool useFermiSurface1 = true;
  helper::testComputeCriticalTemperature<float>(useFermiSurface1);
}

TEST_CASE("Test computeCriticalTemperature<float><float> - FS2") {
  const bool useFermiSurface1 = false;
  helper::testComputeCriticalTemperature<float>(useFermiSurface1);
}

TEST_CASE("Test computeCriticalTemperature<double> - FS1") {
  const bool useFermiSurface1 = true;
  helper::testComputeCriticalTemperature<double>(useFermiSurface1);
}

TEST_CASE("Test computeCriticalTemperature<double> - FS2") {
  const bool useFermiSurface1 = false;
  helper::testComputeCriticalTemperature<double>(useFermiSurface1);
}
