# Research reproducibility
The goal of this folder is to provide scripts and files that can be used to reproduce scientific research (data/results and figures). Of interest is both published research, but also manuscripts under review, since it improves the transparency of the research and review process.

Please see the README in each subfolder for details and instructions.
